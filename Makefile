pushcerts:
	kubectl create secret generic ssl-key -n perfsonar --from-file=cert=cert/k8s_cert.cer --from-file=key=cert/k8s.key

grantpriv:
	kubectl create clusterrolebinding default:psp:privileged \
    --clusterrole=psp:privileged-user \
    --serviceaccount=perfsonar:default \
		-n perfsonar

restarttestpoints:
	kubectl delete pods -l k8s-app=testpoint-h
    kubectl delete pods -l k8s-app=testpoint