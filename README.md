# perfsonar container
## this repo is forked from prp/perfsonar

### List of containers
* pS Toolkit
* pS Testpoint
* Meshconfig
* Cassandra
* Cassandra Monitor
* Traceroute visualization
* Traffic Ingress

To setup a topology of a central archiver and distributed testpoint node, you will at least have the first three containers up.

## Running the pods
### testpoint
Create  the perfsonar namespace
``` bash
kubectl create namespace perfsonar
```
switch to perfsonar namespace. this will save you a lot of time!
If you have not kubectx installed,please refer here: https://github.com/ahmetb/kubectx
```bash
kubens perfsonar
```

Create testpoint/testpoint-h pods on every minion
```bash
kubectl create -f testpoint.yaml
```

Check if the pods are up and running
```bash
kubectl get pods
```

If the status is not running after sometime, you can check the pod logs. You can also refer down below for some common problems.
```bash
kubectl logs <pod_name>
```

### meshconfig
This container is to auto-generate the psconfig template for maddash and pscheduler agents.
```bash
kubectl create -f meshconfig.yaml
```
After the pod is running, you can check if you could get the file from other pods by
```bash
curl meshconfig.perfsonar
```
note: to enable the throughput tests, you need to label your nodes as nw=10G/40G/100G.
```bash
kubectl label nodes <node-name> nw=100G
kubectl get nodes --show-labels
```
### perfsonar-toolkit + cassandra
Cassandra is runing in a separte pod. You can create multiple replicas of Cassandra for safe. refer to the 'cassandra-statefulset.yaml'.
```bash
kubectl create -f toolkit.yaml
kubectl create -f cassandra-statefulset.yaml
```
Both pods should be up and running. Check if all jobs are running fine. Restart it if it exited
```bash
supervisorctl status
```
Now if everything runs okay, you should be able to see the maddash-webui with tests automatically scheduled. there are several ways for you to view it. 1. using the ingress conatiner if you have a DNS name; 2. the toolkit has nodeport configured, you should be able to see it with the port specified.Use the following command to find the port; 3. use port forwarding to accessing directly from master.
```bash
#check nodeport
kubectl get service perfsonar-toolkit
#port-forward cmd
kubectl port-forward <perfsonar-toolkit-pod-name> 8443:443`
```

## Important logs:
pscheduler
```bash
/var/log/pscheduler
```
maddash
```bash
/var/log/perfsonar
```
apache
```bash
/var/log/httpd
```

## Common problem:
### maddash
Normmaly, maddash to fail in toolkit pod because it does not work well with supervisord.
symptom:
```bash
maddash  FATAL  Exited too quickly (process log may have details)
```
solution: run it manually again.
Get into the pod; copy the maddash command; kill the existing process; run the command again.
```bash
kubectl exec -it <pod-name> bash
ps awx | grep java
kill -9 <pid>
<run the copied cmd above>
```
### esmond
symptom: httpd log have unauthorized post code 201
solution: get a api key and update in config.toml
```bash
python esmond/manage.py add_ps_metadata_post_user example_user
python esmond/manage.py add_timeseries_post_user example_user
```
### Cassandra
symptom:observations of following from cassandra containers
```bash
`WARN 23:16:45 CassandraRoleManager skipped default role setup: some nodes were not ready
INFO 23:16:45 Setup task failed with error, rescheduling
```
solution: use the cassandra locally in the same pod. To do that, remove the patch in the toolkit directory.
### Invalid json
symptom: Everything is set but maddash-webui is still empty.
solution: Check the conifg json from meshconfig. You can validete the template by:
```bash
psconfig validte <template-json-link>
```

### Results not post to Esmond
symptom: template added with archiver, tests running but results showing archiver value null from below command from testpoint hosts:
```bash
psconfig pscheduler-tasks
```
solution: check if you templated is added with '--configure-archives' option. you should see the option set to true if you check:
```bash
psconfig remote list
```
Re-add the template if not checked:
```bash
psconfig remote add <template-json-link> --configure-archives
```

### posgres failure
Symptom: observe postgres failure and more bus error when running manually
```bash
#failure from supervisorctl
INFO gave up: initpostgres entered FATAL state, too many start retries too quickly
```
Solution: All the tespoint are running without hughpages. Disable hugepages on the node to resolve the memory map conflict
```bash
echo madvise > /sys/kernel/mm/transparent_hugepage/enabled
```
