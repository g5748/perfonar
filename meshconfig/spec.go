package main

type MeshConfig struct {
	Meta map[string]string `json:"_meta,omitempty"`
	Addresses map[string]Address `json:"addresses,omitempty"`
	Groups map[string]Group `json:"groups,omitempty"`
	Tests map[string]Test `json:"tests,omitempty"`
	Archives map[string]Archive `json:"archives,omitempty"`
	Schedules map[string]Schedule `json:"schedules,omitempty"`
	Tasks map[string]Task `json:"tasks,omitempty"`
}

type Address struct {
	Meta map[string]string `json:"_meta,omitempty"`
	Address string `json:"address,omitempty"`
	Host string `json:"host,omitempty"`
	PschedulerAddress string `json:"pscheduler-address,omitempty"`
	Labels map[string]Address `json:"labels,omitempty"`
}

type Group struct {
	Type      string `json:"type,omitempty"`
	DefaultAddressLabel string `json:"default-address-label,omitempty"`
	Addresses []GroupAddress `json:"addresses,omitempty"`
	AAddresses []GroupAddress `json:"a-addresses,omitempty"`
	BAddresses []GroupAddress `json:"b-addresses,omitempty"`

}

type GroupAddress struct {
	Name string `json:"name,omitempty"`
}

type Test struct {
	Type string `json:"type,omitempty"`
	Spec TestSpec `json:"spec,omitempty"`
}

type TestSpec struct {
	Source string `json:"source,omitempty"`
	Dest string `json:"dest,omitempty"`
	SourceNode string `json:"source-node,omitempty"`
	DestNode string `json:"dest-node,omitempty"`
	Duration string `json:"duration,omitempty"`
	PacketInterval float32 `json:"packet-interval,omitempty"`
	PacketCount int `json:"packet-count,omitempty"`
	Parallel int `json:"parallel,omitempty"`
}

type Archive struct {
	Archiver string `json:"archiver,omitempty"`
	Data map[string]string `json:"data,omitempty"`
}

type Schedule struct {
	Repeat string `json:"repeat,omitempty"`
	Slip string `json:"slip,omitempty"`
	SlipRand bool `json:"sliprand,omitempty"`
}

type Task struct {
	Meta map[string]string `json:"_meta,omitempty"`
	Group string `json:"group,omitempty"`
	Test string `json:"test,omitempty"`
	Archives []string `json:"archives,omitempty"`
	Tools []string `json:"tools,omitempty"`
	Schedule string `json:"schedule,omitempty"`
}