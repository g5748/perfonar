package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/spf13/viper"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

var clientset *kubernetes.Clientset

func getMeshConfig() (MeshConfig, error) {
	conf := MeshConfig{
		Meta: map[string]string{
			"display-name": "MREN Mesh",
		},
		Addresses: map[string]Address{},
		Groups:    map[string]Group{},
		Tests: map[string]Test{
			"latency_test": {
				Type: "latencybg",
				Spec: TestSpec{
					Source:         "{% address[0] %}",
					Dest:           "{% address[1] %}",
					SourceNode:     "{% address[0] %}:9443",
					DestNode:       "{% address[1] %}:9443",
					PacketInterval: 1,
					PacketCount:    600,
				},
			},
			"trace_test": {
				Type: "trace",
				Spec: TestSpec{
					Source:     "{% address[0] %}",
					Dest:       "{% address[1] %}",
					SourceNode: "{% address[0] %}:9443",
				},
			},
			"throughput_test": {
				Type: "throughput",
				Spec: TestSpec{
					Source:     "{% address[0] %}",
					Dest:       "{% address[1] %}",
					SourceNode: "{% address[0] %}:9443",
					DestNode:   "{% address[1] %}:9443",
					Duration:   "PT15S",
					Parallel:   4,
				},
			},
			"throughput_test_100g": {
				Type: "throughput",
				Spec: TestSpec{
					Source:     "{% address[0] %}",
					Dest:       "{% address[1] %}",
					SourceNode: "{% address[0] %}:9443",
					DestNode:   "{% address[1] %}:9443",
					Duration:   "PT15S",
					Parallel:   8,
				},
			},
		},
		Archives: map[string]Archive{
			"esmond_archive": {
				Archiver: "esmond",
				Data: map[string]string{
					"url":               fmt.Sprintf("%s/esmond/perfsonar/archive/", viper.GetString("cluster_url")),
					"measurement-agent": "{% scheduled_by_address %}",
					"_auth-token":       viper.GetString("token"),
				},
			},
		},
		Schedules: map[string]Schedule{
			"every_6_hours": Schedule{
				Repeat:   "PT6H",
				Slip:     "PT6H",
				SlipRand: true,
			},
			"every_12_hours": Schedule{
				Repeat:   "PT12H",
				Slip:     "PT12H",
				SlipRand: true,
			},
		},
		Tasks: map[string]Task{},
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	pods, err := clientset.CoreV1().Pods("perfsonar").List(ctx, metav1.ListOptions{LabelSelector: "k8s-app=testpoint"})
	if err != nil {
		log.Printf("Error getting testpoint pods: %s", err.Error())
		return conf, err
	}

	nodes, err := clientset.CoreV1().Nodes().List(ctx, metav1.ListOptions{})
	if err != nil {
		log.Printf("Error getting testpoint nodes: %s", err.Error())
		return conf, err
	}

	for _, node := range nodes.Items {
		addr := Address{}

		intAddr := Address{}

		for _, adr := range node.Status.Addresses {
			if adr.Type == v1.NodeInternalIP {
				addr.Address = adr.Address
				addr.PschedulerAddress = adr.Address + ":9443"
			}
		}

		for _, pod := range pods.Items {
			if pod.Status.Phase != v1.PodRunning || pod.Status.PodIP == "" {
				continue
			}

			if pod.Status.HostIP == addr.Address {
				intAddr.Address = pod.Status.PodIP
				intAddr.PschedulerAddress = pod.Status.PodIP + ":9443"
			}
		}

		if addr.Address != "" {
			conf.Addresses[node.Name] = addr
		}

		if intAddr.Address != "" {
			conf.Addresses[node.Name+"_internal"] = intAddr
		}

		if node.Labels["mrp/latency"] != "" {
			lat_groups := strings.Split(node.Labels["mrp/latency"], "_")
			for _, lat_group := range lat_groups {
				is_a := false
				is_b := false
				if strings.HasSuffix(lat_group, "-a") {
					lat_group = strings.Replace(lat_group, "-a", "", 1)
					is_a = true
				}
				if strings.HasSuffix(lat_group, "-b") {
					lat_group = strings.Replace(lat_group, "-b", "", 1)
					is_b = true
				}

				if group, ok := conf.Groups["latency_"+lat_group]; !ok {
					newGrp := Group{}

					if is_a {
						newGrp.Type = "disjoint"
						newGrp.AAddresses = []GroupAddress{
							{node.Name + "_internal"},
						}
					} else if is_b {
						newGrp.Type = "disjoint"
						newGrp.BAddresses = []GroupAddress{
							{node.Name + "_internal"},
						}
					} else {
						newGrp.Type = "mesh"
						newGrp.Addresses = []GroupAddress{
							{node.Name + "_internal"},
						}
					}

					conf.Groups["latency_"+lat_group] = newGrp
				} else {
					if is_a {
						group.AAddresses = append(group.AAddresses, GroupAddress{node.Name + "_internal"})
					} else if is_b {
						group.BAddresses = append(group.BAddresses, GroupAddress{node.Name + "_internal"})
					} else {
						group.Addresses = append(group.Addresses, GroupAddress{node.Name + "_internal"})
					}
					conf.Groups["latency_"+lat_group] = group
				}
			}
		}

		if node.Labels["mrp/throughput"] != "" {
			throughput_groups := strings.Split(node.Labels["mrp/throughput"], "_")
			for _, throughput_group := range throughput_groups {
				is_a := false
				is_b := false
				if strings.HasSuffix(throughput_group, "-a") {
					throughput_group = strings.Replace(throughput_group, "-a", "", 1)
					is_a = true
				}
				if strings.HasSuffix(throughput_group, "-b") {
					throughput_group = strings.Replace(throughput_group, "-b", "", 1)
					is_b = true
				}

				if group, ok := conf.Groups["throughput_"+throughput_group]; !ok {
					newGrp := Group{}

					if is_a {
						newGrp.Type = "disjoint"
						newGrp.AAddresses = []GroupAddress{
							{node.Name},
						}
					} else if is_b {
						newGrp.Type = "disjoint"
						newGrp.BAddresses = []GroupAddress{
							{node.Name},
						}
					} else {
						newGrp.Type = "mesh"
						newGrp.Addresses = []GroupAddress{
							{node.Name},
						}
					}

					conf.Groups["throughput_"+throughput_group] = newGrp
				} else {
					if is_a {
						group.AAddresses = append(group.AAddresses, GroupAddress{node.Name})
					} else if is_b {
						group.BAddresses = append(group.BAddresses, GroupAddress{node.Name})
					} else {
						group.Addresses = append(group.Addresses, GroupAddress{node.Name})
					}
					conf.Groups["throughput_"+throughput_group] = group
				}
			}
		}

		if node.Labels["mrp/trace"] != "" {
			trace_groups := strings.Split(node.Labels["mrp/trace"], "_")
			for _, trace_group := range trace_groups {
				is_a := false
				is_b := false
				if strings.HasSuffix(trace_group, "-a") {
					trace_group = strings.Replace(trace_group, "-a", "", 1)
					is_a = true
				}
				if strings.HasSuffix(trace_group, "-b") {
					trace_group = strings.Replace(trace_group, "-b", "", 1)
					is_b = true
				}

				if group, ok := conf.Groups["trace_"+trace_group]; !ok {
					newGrp := Group{}

					if is_a {
						newGrp.Type = "disjoint"
						newGrp.AAddresses = []GroupAddress{
							{node.Name},
						}
					} else if is_b {
						newGrp.Type = "disjoint"
						newGrp.BAddresses = []GroupAddress{
							{node.Name},
						}
					} else {
						newGrp.Type = "mesh"
						newGrp.Addresses = []GroupAddress{
							{node.Name},
						}
					}

					conf.Groups["trace_"+trace_group] = newGrp
				} else {
					if is_a {
						group.AAddresses = append(group.AAddresses, GroupAddress{node.Name})
					} else if is_b {
						group.BAddresses = append(group.BAddresses, GroupAddress{node.Name})
					} else {
						group.Addresses = append(group.Addresses, GroupAddress{node.Name})
					}
					conf.Groups["trace_"+trace_group] = group
				}
			}
		}

	}

	for grpName, _ := range conf.Groups {
		if strings.HasPrefix(grpName, "latency_") {
			conf.Tasks[grpName+"_task"] = Task{
				Group:    grpName,
				Test:     "latency_test",
				Archives: []string{"esmond_archive"},
				Meta: map[string]string{
					"display-name": "Latency " + strings.Replace(grpName, "latency_", "", 1),
				},
			}
		}

		if strings.HasPrefix(grpName, "throughput_") {
			if !strings.Contains(grpName, "100G") {
				conf.Tasks[grpName+"_task"] = Task{
					Group:    grpName,
					Test:     "throughput_test",
					Archives: []string{"esmond_archive"},
					Tools:    []string{"ethr"},
					Schedule: "every_12_hours",
					Meta: map[string]string{
						"display-name": "Throughput " + strings.Replace(grpName, "throughput_", "", 1),
					},
				}
			} else {
				conf.Tasks[grpName+"_task"] = Task{
					Group:    grpName,
					Test:     "throughput_test_100g",
					Archives: []string{"esmond_archive"},
					Tools:    []string{"ethr"},
					Schedule: "every_12_hours",
					Meta: map[string]string{
						"display-name": "Throughput " + strings.Replace(grpName, "throughput_", "", 1),
					},
				}
			}
		}

		if strings.HasPrefix(grpName, "trace_") {
			conf.Tasks[grpName+"_task"] = Task{
				Group:    grpName,
				Test:     "trace_test",
				Archives: []string{"esmond_archive"},
				Tools:    []string{"tracepath"},
				Meta: map[string]string{
					"display-name": "Trace " + strings.Replace(grpName, "trace_", "", 1),
				},
				Schedule: "every_6_hours",
			}
		}
	}

	return conf, nil
}

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	if mesh, err := getMeshConfig(); err != nil {
		w.Write([]byte(err.Error()))
		return
	} else {
		b, err := json.Marshal(mesh)
		if err != nil {
			fmt.Println("error:", err)
			w.Write([]byte(err.Error()))
			return
		}
		w.Write(b)
	}
}

func main() {
	viper.SetConfigName("config")
	viper.AddConfigPath("config")

	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("fatal error config file: %s", err))
	}

	dev := false

	k8sconfig, err := rest.InClusterConfig()
	if err != nil {
		k8sconfig, err = clientcmd.BuildConfigFromFlags("", "config/admin.conf")
		dev = true
		if err != nil {
			panic(err.Error())
		}
	}

	clientset, err = kubernetes.NewForConfig(k8sconfig)
	if err != nil {
		log.Fatal("Failed to do inclusterconfig new client: " + err.Error())
	}

	if dev {
		if mesh, err := getMeshConfig(); err != nil {
			fmt.Printf("Error: %s", err.Error())
			return
		} else {
			b, err := json.Marshal(mesh)
			if err != nil {
				fmt.Println("error:", err)
				fmt.Printf("Error: %s", err.Error())
				return
			}
			fmt.Printf("%s", b)
		}
		return
	}

	http.HandleFunc("/", IndexHandler)
	log.Fatal(http.ListenAndServe(":80", nil))
}
