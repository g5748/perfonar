module gitlab.nautilus.optiputer.net/youf3/perfsonar/meshconfig

go 1.15

require (
	github.com/imdario/mergo v0.3.11 // indirect
	github.com/spf13/viper v1.7.1
	k8s.io/api v0.20.4
	k8s.io/apimachinery v0.20.4
	k8s.io/client-go v0.20.0
	k8s.io/utils v0.0.0-20210111153108-fddb29f9d009 // indirect
)
