package main

import (
	"fmt"
	"net/http"
	"os/exec"
	"time"
)

func handleCORS(h http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Access-Control-Allow-Origin", "*")
		h.ServeHTTP(w, r)
	}
}

func buildGraph() {
	if out, err := exec.Command("/usr/local/bin/python", "/opt/traceroute.py").Output(); err == nil {
		fmt.Printf("%s", out)
	} else {
		fmt.Printf("%s", err.Error())
	}
}

func main() {
	ticker := time.NewTicker(2 * time.Hour)
	go func() {
		buildGraph()
		for {
			select {
			case <-ticker.C:
				buildGraph()
			}
		}
	}()

	http.Handle("/", handleCORS(http.FileServer(http.Dir("/web"))))

	http.ListenAndServe(":80", nil)
}
